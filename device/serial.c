/*
 * @copyright (c) 2023-2024, MR Development Team
 *
 * @license SPDX-License-Identifier: Apache-2.0
 *
 * @date 2023-10-20    MacRsh       First version
 */

#include "include/device/mr_serial.h"

#ifdef MR_USING_SERIAL

static int mr_serial_open(struct mr_dev *dev)
{
    struct mr_serial *serial = (struct mr_serial *)dev;
    struct mr_serial_ops *ops = (struct mr_serial_ops *)dev->drv->ops;

    int ret = mr_ringbuf_allocate(&serial->rd_fifo, serial->rd_bufsz);
    if (ret < 0)
    {
        return ret;
    }
    ret = mr_ringbuf_allocate(&serial->wr_fifo, serial->wr_bufsz);
    if (ret < 0)
    {
        return ret;
    }

    return ops->configure(serial, &serial->config);
}

static int mr_serial_close(struct mr_dev *dev)
{
    struct mr_serial *serial = (struct mr_serial *)dev;
    struct mr_serial_ops *ops = (struct mr_serial_ops *)dev->drv->ops;
    struct mr_serial_config close_config = {0};

    mr_ringbuf_free(&serial->rd_fifo);
    mr_ringbuf_free(&serial->wr_fifo);

    return ops->configure(serial, &close_config);
}

static ssize_t mr_serial_read(struct mr_dev *dev, void *buf, size_t count)
{
    struct mr_serial *serial = (struct mr_serial *)dev;
    struct mr_serial_ops *ops = (struct mr_serial_ops *)dev->drv->ops;
    uint8_t *rd_buf = (uint8_t *)buf;
    ssize_t rd_size;

    if (mr_ringbuf_get_bufsz(&serial->rd_fifo) == 0)
    {
        for (rd_size = 0; rd_size < count; rd_size += sizeof(*rd_buf))
        {
            int ret = ops->read(serial, rd_buf);
            if (ret < 0)
            {
                return (rd_size == 0) ? ret : rd_size;
            }
            rd_buf++;
        }
    } else
    {
        rd_size = (ssize_t)mr_ringbuf_read(&serial->rd_fifo, buf, count);
    }
    return rd_size;
}

static ssize_t mr_serial_write(struct mr_dev *dev, const void *buf, size_t count)
{
    struct mr_serial *serial = (struct mr_serial *)dev;
    struct mr_serial_ops *ops = (struct mr_serial_ops *)dev->drv->ops;
    uint8_t *wr_buf = (uint8_t *)buf;
    ssize_t wr_size;

    if (dev->sync == MR_SYNC)
    {
        for (wr_size = 0; wr_size < count; wr_size += sizeof(*wr_buf))
        {
            int ret = ops->write(serial, *wr_buf);
            if (ret < 0)
            {
                return (wr_size == 0) ? ret : wr_size;
            }
            wr_buf++;
        }
    } else
    {
        wr_size = (ssize_t)mr_ringbuf_write(&serial->wr_fifo, buf, count);
        if (wr_size > 0)
        {
            /* Start interrupt sending */
            ops->start_tx(serial);
        }
    }
    return wr_size;
}

static int mr_serial_ioctl(struct mr_dev *dev, int cmd, void *args)
{
    struct mr_serial *serial = (struct mr_serial *)dev;
    struct mr_serial_ops *ops = (struct mr_serial_ops *)dev->drv->ops;

    switch (cmd)
    {
        case MR_IOC_SERIAL_SET_CONFIG:
        {
            if (args != MR_NULL)
            {
                struct mr_serial_config config = *(struct mr_serial_config *)args;

                int ret = ops->configure(serial, &config);
                if (ret < 0)
                {
                    return ret;
                }
                serial->config = config;
                return sizeof(config);
            }
            return MR_EINVAL;
        }
        case MR_IOC_SERIAL_SET_RD_BUFSZ:
        {
            if (args != MR_NULL)
            {
                size_t bufsz = *(size_t *)args;

                int ret = mr_ringbuf_allocate(&serial->rd_fifo, bufsz);
                serial->rd_bufsz = 0;
                if (ret < 0)
                {
                    return ret;
                }
                serial->rd_bufsz = bufsz;
                return sizeof(bufsz);
            }
            return MR_EINVAL;
        }
        case MR_IOC_SERIAL_SET_WR_BUFSZ:
        {
            if (args != MR_NULL)
            {
                size_t bufsz = *(size_t *)args;

                int ret = mr_ringbuf_allocate(&serial->wr_fifo, bufsz);
                serial->wr_bufsz = 0;
                if (ret < 0)
                {
                    return ret;
                }
                serial->wr_bufsz = bufsz;
                return sizeof(bufsz);
            }
            return MR_EINVAL;
        }
        case MR_IOC_SERIAL_CLR_RD_BUF:
        {
            mr_ringbuf_reset(&serial->rd_fifo);
            return MR_EOK;
        }
        case MR_IOC_SERIAL_CLR_WR_BUF:
        {
            mr_ringbuf_reset(&serial->wr_fifo);
            return MR_EOK;
        }
        case MR_IOC_SERIAL_GET_CONFIG:
        {
            if (args != MR_NULL)
            {
                struct mr_serial_config *config = (struct mr_serial_config *)args;

                *config = serial->config;
                return sizeof(*config);
            }
            return MR_EINVAL;
        }
        case MR_IOC_SERIAL_GET_RD_BUFSZ:
        {
            if (args != MR_NULL)
            {
                size_t *bufsz = (size_t *)args;

                *bufsz = serial->rd_bufsz;
                return sizeof(*bufsz);
            }
            return MR_EINVAL;
        }
        case MR_IOC_SERIAL_GET_WR_BUFSZ:
        {
            if (args != MR_NULL)
            {
                size_t *bufsz = (size_t *)args;

                *bufsz = serial->wr_bufsz;
                return sizeof(*bufsz);
            }
            return MR_EINVAL;
        }
        case MR_IOC_SERIAL_GET_RD_DATASZ:
        {
            if (args != MR_NULL)
            {
                size_t *datasz = (size_t *)args;

                *datasz = mr_ringbuf_get_data_size(&serial->rd_fifo);
                return sizeof(*datasz);
            }
            return MR_EINVAL;
        }
        case MR_IOC_SERIAL_GET_WR_DATASZ:
        {
            if (args != MR_NULL)
            {
                size_t *datasz = (size_t *)args;

                *datasz = mr_ringbuf_get_data_size(&serial->wr_fifo);
                return sizeof(*datasz);
            }
            return MR_EINVAL;
        }
        default:
        {
            return MR_ENOTSUP;
        }
    }
}

static ssize_t mr_serial_isr(struct mr_dev *dev, int event, void *args)
{
    struct mr_serial *serial = (struct mr_serial *)dev;
    struct mr_serial_ops *ops = (struct mr_serial_ops *)dev->drv->ops;

    switch (event)
    {
        case MR_ISR_SERIAL_RD_INT:
        {
            uint8_t data;

            /* Read data to FIFO */
            int ret = ops->read(serial, &data);
            if (ret < 0)
            {
                return ret;
            }
            mr_ringbuf_push_force(&serial->rd_fifo, data);

            return (ssize_t)mr_ringbuf_get_data_size(&serial->rd_fifo);
        }
        case MR_ISR_SERIAL_WR_INT:
        {
            uint8_t data;

            /* Write data from FIFO, if FIFO is empty, stop transmit */
            if (mr_ringbuf_pop(&serial->wr_fifo, &data) == sizeof(data))
            {
                ops->write(serial, data);
                return MR_EBUSY;
            } else
            {
                ops->stop_tx(serial);
                return MR_EOK;
            }
        }
        default:
        {
            return MR_ENOTSUP;
        }
    }
}

/**
 * @brief This function register a serial.
 *
 * @param serial The serial.
 * @param path The path of the serial.
 * @param drv The driver of the serial.
 *
 * @return 0 on success, otherwise an error code.
 */
int mr_serial_register(struct mr_serial *serial, const char *path, struct mr_drv *drv)
{
    static struct mr_dev_ops ops =
        {
            mr_serial_open,
            mr_serial_close,
            mr_serial_read,
            mr_serial_write,
            mr_serial_ioctl,
            mr_serial_isr
        };
    struct mr_serial_config default_config = MR_SERIAL_CONFIG_DEFAULT;

    MR_ASSERT(serial != MR_NULL);
    MR_ASSERT(path != MR_NULL);
    MR_ASSERT(drv != MR_NULL);
    MR_ASSERT(drv->ops != MR_NULL);

    /* Initialize the fields */
    serial->config = default_config;
    mr_ringbuf_init(&serial->rd_fifo, MR_NULL, 0);
    mr_ringbuf_init(&serial->wr_fifo, MR_NULL, 0);
#ifndef MR_CFG_SERIAL_RD_BUFSZ
#define MR_CFG_SERIAL_RD_BUFSZ          (0)
#endif /* MR_CFG_SERIAL_RD_BUFSZ */
#ifndef MR_CFG_SERIAL_WR_BUFSZ
#define MR_CFG_SERIAL_WR_BUFSZ          (0)
#endif /* MR_CFG_SERIAL_WR_BUFSZ */
    serial->rd_bufsz = MR_CFG_SERIAL_RD_BUFSZ;
    serial->wr_bufsz = MR_CFG_SERIAL_WR_BUFSZ;

    /* Register the serial */
    return mr_dev_register(&serial->dev, path, MR_DEV_TYPE_SERIAL, MR_O_RDWR | MR_O_NONBLOCK, &ops, drv);
}

#endif /* MR_USING_SERIAL */
